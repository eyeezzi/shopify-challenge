//
//  Order.swift
//  summarify
//
//  Created by Uzziah on 2018-07-15.
//  Copyright © 2018 eyeezzi. All rights reserved.
//
import Foundation
import SwiftyJSON

struct Order {
    var oid: UInt64
    var number: Int
    var name: String
    var total_price: Double
    var currency: String
    var created_at: Date
    var customer: Customer?
    var shipping_address: Address?
}
extension Order {
    init?(json: JSON) {
        guard
            let _oid = json["id"].uInt64,
            let _number = json["number"].int,
            let _name = json["name"].string,
            let _tp = json["total_price"].string,
            let _total_price = Double(_tp),
            let _currency = json["currency"].string,
            let _crat = json["created_at"].string,
            let _created_at = ISO8601DateFormatter().date(from: _crat)
        else {
            debugPrint(json)
            return nil
        }
        let _cust = json["customer"].dictionaryObject ?? [:]
        let _customer = Customer.init(json: JSON(_cust))
        let _saddr = json["shipping_address"].dictionaryObject ?? [:]
        let _shipping_address = Address.init(json: JSON(_saddr))
        
        self.init(oid: _oid, number: _number, name: _name, total_price: _total_price, currency: _currency, created_at: _created_at, customer: _customer, shipping_address: _shipping_address)
    }
}
