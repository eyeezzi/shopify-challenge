//
//  Address.swift
//  summarify
//
//  Created by Uzziah on 2018-07-16.
//  Copyright © 2018 eyeezzi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Address {
    var name: String
    var province: String
    var country: String
}
extension Address {
    init?(json: JSON) {
        guard
            let _name = json["name"].string,
            let _province = json["province"].string,
            let _country = json["country"].string
            else {
                debugPrint(json)
                return nil
        }
        
        self.init(name: _name, province: _province, country: _country)
    }
}
