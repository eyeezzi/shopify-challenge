//
//  Customer.swift
//  summarify
//
//  Created by Uzziah on 2018-07-16.
//  Copyright © 2018 eyeezzi. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Customer {
    var cid: UInt64
    var email: String
    var first_name: String
    var last_name: String
    var default_address: Address
}
extension Customer {
    init?(json: JSON) {
        guard
            let _cid = json["id"].uInt64,
            let _email = json["email"].string,
            let _fname = json["first_name"].string,
            let _lname = json["last_name"].string,
            let _addr = json["default_address"].dictionary,
            let _address = Address.init(json: JSON(_addr))
            else {
                debugPrint(json)
                return nil
        }
        
        self.init(cid: _cid, email: _email, first_name: _fname, last_name: _lname, default_address: _address)
    }
}
