//
//  OrderByProvinceVC.swift
//  summarify
//
//  Created by Uzziah on 2018-07-16.
//  Copyright © 2018 eyeezzi. All rights reserved.
//

import UIKit

class OrderByProvinceVC: UITableViewController {
    var ordersByProvince: [String: [Order]] = [:]
}
extension OrderByProvinceVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return ordersByProvince.count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ordersByProvince.keys.sorted()[section]
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let province = ordersByProvince.keys.sorted()[section]
        return ordersByProvince[province]?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell2", for: indexPath)
        
        let province = ordersByProvince.keys.sorted()[indexPath.section]
        if let order = ordersByProvince[province]?[indexPath.row] {
            cell.textLabel?.text = "Order: \(order.name)"
            cell.detailTextLabel?.text = "Price: \(order.currency) \(String(format: "%.2f", order.total_price))"
        }
        
        return cell
    }
}
