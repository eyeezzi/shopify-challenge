//
//  SummaryPageVC.swift
//  summarify
//
//  Created by Uzziah on 2018-07-15.
//  Copyright © 2018 eyeezzi. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

class SummaryPageVC: UITableViewController {

    var orders: [Order] = []
    var categories: [String] = ["Orders by Province", "Orders by Year"]
    var ordersByProvince: [String: [Order]] = [:]
    var ordersByYear: [Int: [Order]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchOrders()
    }
    
    func fetchOrders() {
        SVProgressHUD.show()
        Alamofire.request(Constants.ORDERS_ENDPOINT, method: .get)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    guard let _orders = json["orders"].array else { return }
                    
                    self.orders = _orders.compactMap({ Order.init(json: $0) })
                    assert(self.orders.count == _orders.count, "Incomplete # of orders serialized")
                    
                    self.ordersByProvince = self.groupOrdersByProvince(orders: self.orders)
                    self.ordersByYear = self.groupOrdersByYear(orders: self.orders)
                    self.tableView.reloadData()
                    SVProgressHUD.showSuccess(withStatus: nil)
                    SVProgressHUD.dismiss(withDelay: 1)
                case .failure(let error):
                    print(error)
                    SVProgressHUD.showError(withStatus: nil)
                    SVProgressHUD.dismiss(withDelay: 1)
                }
            }
    }
    
    func groupOrdersByProvince(orders: [Order]) -> [String: [Order]] {
        
        var dict: [String: [Order]] = [:]
        
        for order in orders {
            // only interested in orders with a shipping address
            guard let province = order.shipping_address?.province else { continue }
            if dict[province] == nil {
                dict[province] = [order]
            } else {
                dict[province]?.append(order)
            }
        }
        return dict
    }
    
    func groupOrdersByYear(orders: [Order]) -> [Int: [Order]] {
        // Sorting will help statisfy Requirement "Target 1" - first 10 orders.
        let sortedOrders = orders.sorted(by: { $0.created_at < $1.created_at })
        
        var dict: [Int: [Order]] = [:]
        for order in sortedOrders {
            let creationYear = Calendar.current.component(.year, from: order.created_at)
            if dict[creationYear] == nil {
                dict[creationYear] = [order]
            } else {
                dict[creationYear]?.append(order)
            }
        }
        return dict
    }
}

extension SummaryPageVC {

    @objc func showDetail(sender: UIButton) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OrderByProvinceVC") as? OrderByProvinceVC else { return }
        vc.ordersByProvince = ordersByProvince
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let button = UIButton()
        button.setTitle(categories[section], for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        button.backgroundColor = UIColor.init(white: 0.85, alpha: 1)
        if section == 0 {
            button.addTarget(self, action: #selector(showDetail(sender:)), for: .touchUpInside)
        }
        if section == 1 {
            button.setTitle("\(categories[section]) (\(ordersByYear[2017]?.count ?? 0) in 2017)", for: .normal)
        }
        return button
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return ordersByProvince.count
        default:
            guard let count2017 = ordersByYear[2017]?.count else { return 0 }
            return count2017 > 10 ? 10 : count2017
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)
            
            let provinceKey = ordersByProvince.keys.sorted()[indexPath.row]
            
            cell.textLabel?.text = "\(ordersByProvince[provinceKey]?.count ?? 0) from \(provinceKey)"
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell2", for: indexPath)
            
            if let order = ordersByYear[2017]?[indexPath.row] {
                cell.textLabel?.text = "Order: \(order.name)"
                cell.detailTextLabel?.text = "Price: \(order.currency) \(String(format: "%.2f", order.total_price))"
            }
            
            return cell
        default:
            fatalError("Unknown section")
        }
    }
}
