# Summarify: A Shopify Challenge

Proposed solution for the Spotify [iOS Mobile Intern Challenge 2018](https://docs.google.com/document/d/1HtmhNOrrM7m5QTxNDGyNUcevZK4H6xjZ3ARSCdSfVuA/edit)

## Dev Dependencies

- Git
- Xcode 9.4.x with Swift4
- Cocoapods 1.5.x

## Setup

	git clone <repo>
	cd <projectDir>
	pod install
	[open .xcworkspace file]
	
## Scope Assumptions

1. Even though app has support for grouping orders by year of creation, only the orders for 2017 are listed in the *Orders by Year* category.
2. Extra 1: As a consequence of *Assumption 1*, the **first 10 orders** is interpreted to mean the **first 10 orders in 2017**.
3. Interpretation of Entity Relationships from description and provided data ![](docs/shopify-challenge-erd.png) 

## Known Limitations

1. No catching strategy when internet connection is not available.
2. Proper UI notice when connection fails or content is empty.
3. A General Sleek UI
	
## 3rd-Party Packages

1. [Alamofire](https://github.com/Alamofire/Alamofire) - For handling HTTP requests and response.
2. [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) - For easier handling of JSON objects from API calls.
3. [SVProgressHUD](https://www.cocoacontrols.com/controls/svprogresshud) - Progress indicator for async tasks.
